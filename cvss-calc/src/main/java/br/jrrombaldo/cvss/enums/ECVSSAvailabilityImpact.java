package br.jrrombaldo.cvss.enums;

public enum ECVSSAvailabilityImpact
{
	N('N', "None"), 
	P('P', "Partial"), 
	C('C', "Complete"), 
	U('U', "Unknown"); 

	private Character code;
	private String literal;

	private ECVSSAvailabilityImpact (final Character code, final String literal)
	{
		this.code = code;
		this.literal = literal;
	}

	public Character getCode ()
	{
		return code;
	}

	public String getLiteral ()
	{
		return literal;
	}

}
