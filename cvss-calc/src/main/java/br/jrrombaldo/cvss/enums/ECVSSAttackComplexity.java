package br.jrrombaldo.cvss.enums;

public  enum ECVSSAttackComplexity{

	L('L', "Low"), 
	M('M', "Medium"), 
	H('H', "High");

	private Character code;
	private String literal;

	private ECVSSAttackComplexity (final Character code, final String literal)
	{
		this.code = code;
		this.literal = literal;
	}

	public Character getCode ()
	{
		return code;
	}

	public String getLiteral ()
	{
		return literal;
	}

}
