package br.jrrombaldo.cvss.enums;

public enum ECVSSAttackVector
{
	L('L', "Local"),
	A('A', "Adjacent Network"), 
	N('N', "Network"), 
	U('U', "Unknown"); 

	private Character code;
	private String literal;

	private ECVSSAttackVector (final Character code, final String literal)
	{
		this.code = code;
		this.literal = literal;
	}

	public Character getCode ()
	{
		return code;
	}

	public String getLiteral ()
	{
		return literal;
	}


}
