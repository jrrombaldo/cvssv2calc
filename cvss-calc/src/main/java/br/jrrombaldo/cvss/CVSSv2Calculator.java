package br.jrrombaldo.cvss;

import br.jrrombaldo.cvss.enums.ECVSSAttackComplexity;
import br.jrrombaldo.cvss.enums.ECVSSAttackVector;
import br.jrrombaldo.cvss.enums.ECVSSAuthLevel;
import br.jrrombaldo.cvss.enums.ECVSSAvailabilityImpact;
import br.jrrombaldo.cvss.enums.ECVSSConfidencialImpact;
import br.jrrombaldo.cvss.enums.ECVSSIntegrityImpact;


/**
 * Calculator for CVSS based on version 2
 *
 */
public class CVSSv2Calculator {
    private double AVv = 0;
    private double ACv = 0;
    private double AUv = 0;
    private double Cv = 0;
    private double Iv = 0;
    private double Av = 0;

    private double impact = 0;
    private double exploitability = 0;
    private double score = 0;

    /**
     * String in standard version, ex: CVSS2#AV:L/AC:L/Au:N/C:P/I:P/A:P
     */
    private String standard;

    public CVSSv2Calculator(ECVSSAttackVector av, ECVSSAttackComplexity ac,
	    ECVSSAuthLevel au, ECVSSConfidencialImpact c,
	    ECVSSIntegrityImpact i, ECVSSAvailabilityImpact a) {

	switch (av) {

	case N:
	    AVv = 1.0;
	    break;
	case A:
	    AVv = .646;
	    break;
	case L:
	    AVv = .395;
	    break;
	default:
	    AVv = 0;
	    break;
	}

	switch (ac) {
	case H:
	    ACv = 0.35;
	    break;
	case M:
	    ACv = 0.61;
	    break;
	case L:
	    ACv = 0.71;
	    break;
	default:
	    ACv = 0;
	    break;
	}

	switch (au) {
	case N:
	    AUv = 0.704;
	    break;
	case S:
	    AUv = 0.56;
	    break;
	case M:
	    AUv = 0.45;
	    break;
	default:
	    AUv = 0;
	    break;
	}

	switch (c) {
	case C:
	    Cv = .66;
	    break;
	case P:
	    Cv = .275;
	    break;
	case N:
	    Cv = 0;
	    break;
	default:
	    Cv = 0;
	    break;
	}

	switch (i) {
	case C:
	    Iv = .66;
	    break;
	case P:
	    Iv = .275;
	    break;
	case N:
	    Iv = 0;
	    break;
	default:
	    Iv = 0;
	    break;
	}

	switch (a) {
	case C:
	    Av = .66;
	    break;
	case P:
	    Av = .275;
	    break;
	case N:
	    Av = 0;
	    break;
	default:
	    Av = 0;
	    break;
	}

	this.impact = 10.41 * (1 - (1 - Cv) * (1 - Iv) * (1 - Av));
	this.exploitability = 20 * AVv * ACv * AUv;
	this.score = (0.6 * impact + 0.4 * exploitability - 1.5)
		* (impact == 0 ? 0 : 1.176);
	this.score = Math.round(score * 10) / 10;

	standard = "AV:<b>" + av + "</b>/AC:<b>" + ac + "</b>/Au:<b>" + au
		+ "</b>/C:<b>" + c + "</b>/I:<b>" + i + "</b>/A:<b>" + a
		+ "</b>";

    }

    public double getScore() {
	return this.score;
    }

    public double getImpact() {
	return this.impact;
    }

    public double getExploitability() {
	return this.exploitability;
    }

    public String getStr() {
	return this.standard;
    }

    @Override
    public String toString() {
	return this.getStr();
    }
}
