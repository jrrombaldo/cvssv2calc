package br.jrrombaldo.cvss.enums;

public enum ECVSSAuthLevel
{
	N('N', "None"), 
	S('S', "Single"), 
	M('M', "Multiple"), 
	U('U', "Unknown"); 

	private Character code;
	private String literal;

	private ECVSSAuthLevel (final Character code, final String literal)
	{
		this.code = code;
		this.literal = literal;
	}

	public Character getCode ()
	{
		return code;
	}

	public String getLiteral ()
	{
		return literal;
	}

}
